package coinpurse;
/**
 * BankNotes with value and serial number ,........
 * @author Nattapat Sukpootanan
 * */
public class BankNote extends AbstractValuable {
	private long serialNumber;
	private static long nextSerialNumber = 100000;
	/**
	 * Constructor for a new coin. 
     * @param value is the value for the coin
	 * */
	public BankNote(double value,String currency){
		 super(value,currency);
		serialNumber= this.getNextSerialNumber();
		
		
	}

   /**
    * output String format.
    * @return String format
    * */
    public String toString(){
    	return this.getValue() + " "+super.getCurrency()+" BankNote";
    }
    /**
     * getNextSerialNumber for run the serial number of BankNote.
     * @return return nextSerialNumber
     * */
    public static long getNextSerialNumber(){
    	return nextSerialNumber++;
    }
}
