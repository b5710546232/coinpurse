package coinpurse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.WithdrawStrategy;

/**
 * A purse contains valuables. You can insert coins BankNote or Coupon, withdraw
 * valuables, check the balance, and check if the purse is full. When you
 * withdraw valuables, the coin purse decides which coins to remove.
 * 
 * @author Nattapat Sukpootanan
 */
public class Purse extends java.util.Observable{
	/** Collection of coins in the purse. */
	private List<Valuable> valuables;
	/**
	 * Capacity is maximum NUMBER of Valuable the purse can hold. Capacity is
	 * set when the purse is created.
	 */
	private WithdrawStrategy strategy;
	private int capacity;
	private final ValueComparator comparator = new ValueComparator();

	/**
	 * Create a purse with a specified capacity.
	 * 
	 * @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse(int capacity) {
		// TODO initialize the attributes of purse
		this.capacity = capacity;
		valuables = new ArrayList<Valuable>();

	}

	/**
	 * Count and return the number of coins in the purse. This is the number of
	 * coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() {
		int num = 0;
		for (int i = 0; i < valuables.size(); i++) {
			num++;
		}
		return num;
	}

	/**
	 * Get the total value of all items in the purse.
	 * @return the total value of items in the purse.
	 */
	public double getBalance() {
		double balance = 0;
		for (int i = 0; i < valuables.size(); i++) {
			balance += valuables.get(i).getValue();
		}
		return balance;
	}

	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */

	public int getCapacity() {
		return this.capacity;
	}

	/**
	 * Test whether the purse is full. The purse is full if number of items in
	 * purse equals or greater than the purse capacity.
	 * @return true if purse is full.
	 */
	public boolean isFull() {

		if (this.count() == this.getCapacity()) {
			return true;
		} else

			return false;
	}

	/**
	 * Insert a coin into the purse. The valuables is only inserted if the purse
	 * has space for it and the coin has positive value. No worthless coins!
	 * 
	 * @param valuables is a Valuable object to insert into purse
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert(Valuable valuables) {
		// if the purse is already full then can't insert anything.

		if (valuables.getValue() <= 0) {
			return false;
		} else {
			if (this.isFull()) {
				return false;
			} else {
				this.valuables.add(valuables);
				super.setChanged();
				super.notifyObservers(this);
				return true;
			}
		}
	}

	/**
	 * Withdraw the requested amount of valuables. remove the list that get from withdraw strategy
	 * @param amount is the amount to withdraw
	 * @return array of valuables that can withdraw, or null if cannot withdraw requested amount.
	 */

	public Valuable[] withdraw(double amount) {
		Collections.sort(valuables, comparator);
		Collections.reverse(valuables);

		Valuable[] remain = strategy.withdraw(amount, valuables);
		int numCount = valuables.size();
		if (remain == null)
			return null;
		for (int i = 0; i < remain.length; i++) {

			for (int j = 0; j < numCount; j++) {

				if (valuables.get(j).equals(remain[i])) {

					valuables.remove(j);

					numCount--;

					break;

				}

			}

		}
		return remain;
	}

	public void setWithdrawStrategy(WithdrawStrategy strategy) {
		this.strategy = strategy;
	}

	/**
	 * toString returns a string description of the purse contents. It can
	 * @return whatever is a useful description.
	 */
	public String toString() {

		int sum = 0;
		for (int i = 0; i < valuables.size(); i++) {
			sum += valuables.get(i).getValue();
		}
		return valuables.size() + "coins with value " + sum;
	}

}