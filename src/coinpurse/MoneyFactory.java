package coinpurse;

import java.util.ResourceBundle;
/**
 * @author nattapat 
 * MoneyFactory select local Factory to created.
 * */
public abstract class MoneyFactory {
	private static ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
	private static MoneyFactory instance;
	public MoneyFactory(){
		
	}
	/**
	 * get instance the MoneyFactory.
	 * @return instance of MoneyFactory.
	 * */
	public static MoneyFactory getInstance(){
		if(instance == null ){
			setMoneyFactory("MalaiMoneyFactory");
			}
		return instance; 
	}
	/**
	 * set the local Factory for MoneyFactory.
	 * @param factory local Factory in String for setting MoneyFactory
	 * */
	public static void setMoneyFactory( String factory){
		String factoryclass = bundle.getString(factory);
			 try {
				instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
			} catch (InstantiationException | IllegalAccessException
					| ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}
	/**
	 * create money
	 * @param value for create money.
	 * @return Valuable money.
	 * */
	public abstract Valuable createMoney(double value);
	/**
	 * create money
	 * @param value in String for create money.
	 * @return Valuable money.
	 * */
	public Valuable createMoney(String value){
		return createMoney(Double.parseDouble(value));
	}
}
