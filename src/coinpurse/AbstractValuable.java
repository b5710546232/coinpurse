package coinpurse;
/**
 * AbstractValuable that have method equals and compare
 * @author Nattapat Sukpootanan.
 * */
public abstract class AbstractValuable implements  Valuable{
	  /** Value of the Valuable. */
    private double value ;
    private String currency;
	public AbstractValuable(double value,String currency) {
		this.value = value;
		this.currency = currency;
	}

	/**
	 * equals if value of coin is equals.
	 * @param obj is other Valuable
	 * @return true if it is equals ,false if it is n't equal
	 * */
	public boolean equals(Object obj) {
	if(this.getClass()==obj.getClass()){
		Valuable other = (Valuable)obj;
		if(other.getValue()==this.getValue()){
			return true;
		}
		return false;
	}
		                                 
		return false;
	}
	  /**
     * Compare Valuable by value.
     * @param other is Valuable to compare to this
     * @return -1 if Valuable  is lower value,0 if Valuable is equals ,1 if Valuable  is higher
     * @throws NullPointerException if Valuable is null
     * @see java.lang.Comparable#compareTo(Object) 
     */
    public int compareTo(Valuable other) {
    	if (other == null) return -1;
    	if(this.equals(other)){
    		return 0;
    	}
    	if(this.getValue()<other.getValue()){
    		return -1;
    	}
    	else return 1; 	
    }
    /**
 	 * Get the value of Valuable.
 	 * @return the value of Valuable
 	 */
     public double getValue(){
     	return this.value;
     }
     public String getCurrency(){
    	 return this.currency;
     }
   

}