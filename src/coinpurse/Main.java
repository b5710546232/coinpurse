package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Nattapat Sukpootanan
 */
public class Main {
//	private static int CAPACITY = 10;
    /**
     * @param args not used
     */
	
    public static void main( String[] args ) {
    	
    	GreedyWithdraw g = new   GreedyWithdraw ();
    	RecursiveWithdraw r = new RecursiveWithdraw();
    	PurseBalanceGUI balanceGUI = new  PurseBalanceGUI();
    	PurseStatusGUI statusGUI = new PurseStatusGUI();
        // 1. create a Purse
    	Purse purse = new Purse(10);
    	purse.setWithdrawStrategy(r);
    	// add balanceGUI.
    	purse.addObserver(balanceGUI);
    	// add status in 
    	purse.addObserver(statusGUI);
        // 2. create a ConsoleDialog with a reference to the Purse object
    	ConsoleDialog consoleDialog = new ConsoleDialog(purse);
        // 3. run() the ConsoleDialog
    	consoleDialog.run();
    	
    	

    }
}
