package coinpurse;

public class RecursionPractice {
	public static void main(String[] agrs){
		double [] x ={1,2,3,4};
		System.out.print(sum(x));
	}

	public static  double sumTo(double [] x, int lastIndex){
		if(lastIndex < 0) return 0.0;
		double sum = x[lastIndex]+sumTo(x,lastIndex-1);
		return sum;
	}
	public static double sum(double [] array){
		return sumTo(array,array.length-1);
	}
}
