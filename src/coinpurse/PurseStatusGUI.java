package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * @author Nattapat Sukpootanan
 * PurseStatusGUI for show GUI in status bar.
 * */

public class PurseStatusGUI extends JFrame implements Observer{
	private JLabel labelStatus;
	private JProgressBar statusBar;
	/**
	 * PurseStatusGUI - Constructor for Create GUI and Component.
	 * */
	public PurseStatusGUI(){
		this.initComponent();
		
	}
	/**
	 * initComponent - initialize all Component in GUI.
	 * */
	public void initComponent(){
		JPanel pane = new JPanel();
		labelStatus = new JLabel("Empty");
		statusBar = new JProgressBar();
		pane.add(labelStatus);
		pane.add(statusBar);
		super.setTitle("Purse Status");
		super.add(pane);
		super.setSize(200,100);
		super.setVisible(true);
		
	}
	/**
	 * update the items in Purse and progress bar.
	 * @param subject is purse that used.
	 * @param info no used.
	 * */
	@Override
	
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			int count = purse.count();
			statusBar.setMaximum(purse.getCapacity());
			statusBar.setValue(count);
			if(purse.getCapacity()==count){
				this.labelStatus.setText("Full");
			}
			else{
				this.labelStatus.setText("Your items = "+purse.count());
			}
			
			
			
		}
		if(info != null) System.out.println(info);
		
	}
}
