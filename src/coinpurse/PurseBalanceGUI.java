package coinpurse;

import java.util.Observable;
import java.util.Observer;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * @author Nattapat Sukpootanan
 * PurseBalanceGUI for Show Balance in GUI.
 * */
public class PurseBalanceGUI extends JFrame implements Observer{
	JLabel labelBalance;
	public PurseBalanceGUI(){
		this.initComponet();
	}
	/**
	 * initComponet - initialize all Components in this GUI.
	 * */
	public void initComponet(){
		JPanel pane = new JPanel();
		labelBalance = new JLabel("0 Baht");
		pane.add(labelBalance);
		super.setTitle("Purse Balance");
		super.add(pane);
		super.setSize(200,100);
		//super.pack();
		super.setVisible(true);
	}
	/**
	 * update - update balance and show in GUI.
	 * @param subject is purse that used.
	 * @param info no used.
	 * */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			int balance = (int)purse.getBalance();
			System.out.println("Balance is: "+balance);
			this.labelBalance.setText(balance+" Baht");


		}
		if(info != null) System.out.println(info);

	}

}
