package coinpurse;
/**
 * This interface about value . 
 * @author Nattapat Sukpootanan */
public interface Valuable extends Comparable<Valuable> {
    /** method to get value.
     * @return the value of Object
     */
     public double getValue();;

}