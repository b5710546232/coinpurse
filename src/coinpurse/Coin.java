package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin. 
 * @author Nattapat Sukpootanan 
 */
public class Coin extends AbstractValuable {


    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ,String currency) {
      super(value,currency);
    }
 
 
    /**
     * toString to output in to String format.
     * @return return value of coin in String 
     */
    public String toString(){
    	return this.getValue()+" "+super.getCurrency()+" coin";
    }
    
    
}

