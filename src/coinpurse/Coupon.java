package coinpurse;
/**
 * A Coupon with color and value. 
 * @author Nattapat Sukpootnan
 * */
public class Coupon extends AbstractValuable  implements Valuable
{  /** Color of Coupon. */
	private String color;
	/**enum select Coupon type and value.*/
	public enum CouponType { RED(100),BLUE(50),GREEN(20);
	
	/**value of Coupon. */
	private int value;
	
	/**for get Coupon value
	 * @param value of each coupon*/
	private CouponType(int value) {
        this.value = value;
}
	}
	/**Constructor for a new Coupon.
	 * @param color of Coupon 
	 * */
	public Coupon(String color){
		super(CouponType.valueOf(color.toUpperCase()).value);
		this.color = color;
		
	}

    /**
     * toString to output in to String format.
     * @return color of String.
     * */
    public String toString(){
    	return this.color+" coupon";
    }
}
