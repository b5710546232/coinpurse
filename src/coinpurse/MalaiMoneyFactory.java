package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * @author nattapat
 * MalaiMoneyFactory create money in Malaysia.
 * */

public class MalaiMoneyFactory extends MoneyFactory{
	@Override
	/**
	 * create money in Malaysia.
	 * @param value for create money.
	 * @return money in Valuable.
	 * */
	public Valuable createMoney(double value) {
		List <Double> coins = Arrays.asList(0.05,0.10,0.50,0.20);
		List <Double> banknotes = Arrays.asList(1.0,2.0,5.0,10.0,20.0,50.0,100.0);
		if(coins.contains(new Double(value)))
			 return  new Coin(value*100,"Sen");
		else if(banknotes.contains(new Double(value))) return  new BankNote(value,"Ringgit");
		else{
			 throw new java.lang.IllegalArgumentException();
		}
	}
}
