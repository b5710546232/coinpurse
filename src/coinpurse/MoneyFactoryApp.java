package coinpurse;

public class MoneyFactoryApp {
	public static void main(String[] args) {
		MoneyFactory factory = MoneyFactory.getInstance();
		System.out.println( factory.createMoney("10") );
		System.out.println( factory.createMoney(50.0) );
	}
}
