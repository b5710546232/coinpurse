package coinpurse;

import java.util.Comparator;
/**
 * comparator the value.
 * @author Nattapat Sukpootanan 
 * */
public class ValueComparator implements Comparator<Valuable>
{ 	/**
	*compare value a and b.
	*@param Valuable  a and b
	*@return value if a<b return -1,a>b return 1,equals return 0.
 	*/
	public int compare(Valuable a, Valuable b) {
	if(a.getValue()>b.getValue()) return 1;
	if(a.getValue()<b.getValue()) return -1;
	else return 0;
	}
}