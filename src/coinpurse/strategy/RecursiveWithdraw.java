package coinpurse.strategy;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import coinpurse.Coin;
import coinpurse.Valuable;
/**
 * @author Nattapat Sukpootanan A strategy that use recursive for withdraw.
 * */
public class RecursiveWithdraw implements WithdrawStrategy
{
	// @Override
	/**
	 * this withdraw get strategy from findWithdraw and get output in array 
	 * @param amount for withdraw
	 * @param valuables of money in purse
	 * @return valuables in array that want to withdraw,return null if can't withdraw.
	 * */
	public  Valuable[] withdraw(double amount, List<Valuable> valuables) {
		if (findWithdraw(amount, valuables, 0) == null) {
			return null;
		}
		Valuable[] values = new Valuable[findWithdraw(amount, valuables, 0).size()];
		for (int i = 0; i < values.length; i++) {
			values[i] = findWithdraw(amount, valuables, 0).get(i);
			System.out.println(Array.get(values, i));
		}
		return values;
	}
	/** 
	 * this is helper method for find valuables that can withdraw withrecursive.
	 * @param amount - amount that want to withdraw
	 * @param valuables list of money in purse
	 * @param start index of list
	 * */
	private List<Valuable> findWithdraw(double amount,List<Valuable> valuables, int start) {
		if (amount < 0)
			return null;
		if (start >= valuables.size())
			return null;
		if (amount - valuables.get(start).getValue() == 0) {
			List<Valuable> list1 = new ArrayList();
			list1.add(valuables.get(start));
			return list1;
		}
		List<Valuable> list2 = findWithdraw(amount - valuables.get(start).getValue(), valuables, start+1);
		if (list2 != null) {
			list2.add(valuables.get(start));
		} else {
			list2 = findWithdraw(amount, valuables, start + 1);
		}
		return list2;
	}
}
