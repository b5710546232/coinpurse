package coinpurse.strategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;
/**
 *  GreedyWithdraw use GreeAlgrorithm for withdraw
 *  @author Nattapat Sukpootanan
 */
public class GreedyWithdraw implements WithdrawStrategy{

	//@Override
	/**  
	 *  GreedyWithdraw the requested amount of value.
	 *  @param amount is the amount to withdraw
	 *  @return array of valuables for remove in Purse 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount,List<Valuable>valuables) {
		ArrayList<Valuable> templist = new ArrayList<Valuable>();

		for(int i =0;i<valuables.size();i++){

			if(amount>=valuables.get(i).getValue()){

				amount-=valuables.get(i).getValue();

				templist.add(valuables.get(i));

			}
		}

		Valuable [] array = new Valuable[ templist.size()];

		for(int i=0;i<templist.size();i++){

			array[i] = templist.get(i);

		}
		if ( amount > 0 )

		{
			return null;

		}

		else {
			return array;
		}
	}



}
