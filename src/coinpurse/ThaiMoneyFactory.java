package coinpurse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
/**
 * @author nattapat 
 * ThaiMoneyFactory for create money in Thai.
 * */

public class ThaiMoneyFactory extends MoneyFactory{
	@Override
	/**
	 * create money in Thai.
	 * @param value for create money.
	 * @return money in Valuable.
	 * */
	public Valuable createMoney(double value) {
		List <Double> coins = Arrays.asList(1.0,2.0,5.0,10.0);
		List <Double> banknotes = Arrays.asList(20.0,50.0,100.0,500.0,1000.0);
		if(coins.contains(new Double(value)))
			 return  new Coin(value,"Bath");
		else if(banknotes.contains(new Double(value))) return  new BankNote(value,"Baht");
		else{
			 throw new java.lang.IllegalArgumentException();
		}
	}
}
