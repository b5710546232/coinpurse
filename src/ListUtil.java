import java.util.Arrays;
import java.util.List;


public class ListUtil {	
	private static void printList(List<?> list){
		System.out.print(printTo(list,0));
	}
	public static String printTo(List list,int startIndex){
		
		if(startIndex>list.size()-1) return "";
		String word = list.get(startIndex) +" "+printTo(list, startIndex+1);
		return word;
	}
	public static String max(List<String> list){
		return findMax(list,list.size()-1,0);
		
	}
	public static String findMax(List<String> list, int index,int max){
	    if (index < 0) return list.get(max);
	    if(list.get(max).compareTo(list.get(index))<=0) max = index;
	    return findMax(list,index-1,max);
	    
	}
	public static int fac(int n){
		 int fact;
		if(n==0) fact =1;
		else fact = n*fac( n-1);
		return fact;
	}
	public static String re(String str){
		int index = str.length();
		String word = ""+str.substring(0,1);
		if(index==0) return "";
				return word + re(str.substring(0,str.length()-1));
	}
	public static String changeXY(String str) {
		  if(str.length()<1){ 
			 // System.out.println(str);
			  return str;
		  }
		  if(str.charAt(0)=='x') {
			 // System.out.println("y" + changeXY(str.substring(1)));
			  return "y" + changeXY(str.substring(1));
		  }
		 // System.out.println(str.charAt(0) + changeXY(str.substring(1)));
		  return str.charAt(0) + changeXY(str.substring(1));
		  
		}

	public static void main(String[] agrs){
		System.out.print(changeXY("xhixhix"));
		//System.out.println(re("1234"));
		System.out.print("fac 5 "+fac(5));
		List <String> list;
		if(agrs.length>0)list = Arrays.asList(agrs);
		else list = Arrays.asList("a","b","c","d");
		System.out.print("List conains: ");
		printList(list);
		String max = max(list);
		System.out.println("Lexically greatest element is "+max);
	}
}
